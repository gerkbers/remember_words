package com.gerkbers.remeberwords.view.main;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.gerkbers.remeberwords.R;

import java.util.List;

/**
 * Created by k.bersenev on 09.11.2017.
 * com.gerkbers.remeberwords.View.Main
 */

public class MainAdapter extends RecyclerView.Adapter<MainHolder> {

    private List<CategoryVTO> mList;
    private Context mContext;

    public MainAdapter(Context context, List<CategoryVTO> list) {
        mList = list;
        mContext = context;
    }

    @Override
    public MainHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MainHolder(LayoutInflater.from(mContext).inflate(R.layout.item_main, parent, false));
    }

    @Override
    public void onBindViewHolder(MainHolder holder, int position) {
        holder.bind(mList.get(position));
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }
}
