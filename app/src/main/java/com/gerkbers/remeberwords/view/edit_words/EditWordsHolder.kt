package com.gerkbers.remeberwords.view.edit_words

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.gerkbers.remeberwords.application.MyApp
import com.gerkbers.remeberwords.presenter.edit_words_presenter.EditWordsPresenter
import com.gerkbers.remeberwords.presenter.WordVTO
import com.gerkbers.remeberwords.R
import javax.inject.Inject

/**
 * Created by k.bersenev on 10.11.2017.
 * com.gerkbers.remeberwords.View.EditWords
 */
class EditWordsHolder(view : View) : RecyclerView.ViewHolder(view) {

    @Inject
    lateinit var presenter : EditWordsPresenter

    @BindView(R.id.editWordsItemName)
    lateinit var name : TextView

    @BindView(R.id.EditWordsItemDelete)
    lateinit var deleteBtn : ImageView

    var id : Long = 0

    init {
        MyApp.sComponentHolder.wordsComponent.inject(this)
        ButterKnife.bind(this, view)
        view.setOnClickListener {
            presenter.onClickWord(id)
        }
        deleteBtn.setOnClickListener{
            presenter.onRemoveWord(id)
        }

    }

    fun bind(word : WordVTO) {
        id = word.id
        name.setText(word.name)
    }
}