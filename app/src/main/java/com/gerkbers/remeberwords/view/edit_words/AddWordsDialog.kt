package com.gerkbers.remeberwords.view.edit_words

import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.widget.EditText
import com.gerkbers.remeberwords.application.MyApp
import com.gerkbers.remeberwords.presenter.edit_words_presenter.EditWordsPresenter
import com.gerkbers.remeberwords.R
import javax.inject.Inject

/**
 * Created by k.bersenev on 13.11.2017.
 * com.gerkbers.remeberwords.View.EditWords
 */
class AddWordsDialog : DialogFragment(){

    @Inject
    lateinit var presenter : EditWordsPresenter

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        MyApp.sComponentHolder.wordsComponent.inject(this)
        val view = LayoutInflater.from(context)
                .inflate(R.layout.dialog_add_words, null, false)

        val englishEdt = view.findViewById<EditText>(R.id.editWordDialogEnglishWord)

        val russianEdt = view.findViewById<EditText>(R.id.editWordDialogRussianWord)

        val transcriptionEdt = view.findViewById<EditText>(R.id.editWordDialogTranscription)

        val dialogBuilder = AlertDialog.Builder(context!!)
                .setTitle(R.string.add_word)
                .setView(view)
                .setPositiveButton(R.string.add_word_button, { dialog, which ->
                    presenter.onAddWord(
                            englishEdt.text.toString(),
                            russianEdt.text.toString(),
                            transcriptionEdt.text.toString()
                    )
                })
                .setNegativeButton(R.string.cancel_add_word_button, null)

        return dialogBuilder.create()
    }
}