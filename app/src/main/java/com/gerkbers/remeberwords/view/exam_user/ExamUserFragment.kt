package com.gerkbers.remeberwords.view.exam_user

import android.os.Bundle
import android.speech.tts.TextToSpeech
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.gerkbers.remeberwords.application.MyApp
import com.gerkbers.remeberwords.MainActivity
import com.gerkbers.remeberwords.presenter.exam_user_presenter.ExamUserPresenter
import com.gerkbers.remeberwords.R
import java.util.*
import javax.inject.Inject

/**
 * Created by k.bersenev on 13.11.2017.
 * com.gerkbers.remeberwords.View.ExamUser
 */
class ExamUserFragment : Fragment(), TextToSpeech.OnInitListener{
    override fun onInit(status: Int) {
        if (status == TextToSpeech.SUCCESS) {

            val locale = Locale("eng")

            val result = tts.setLanguage(locale)
            //int result = mTTS.setLanguage(Locale.getDefault());

            if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
            } else {
                canSpeak = true
            }

        } else {

        }
    }

    @BindView(R.id.examContent)
    lateinit var examContent : LinearLayout

    @BindView(R.id.russian_word)
    lateinit var russianWordTV : TextView

    @BindView(R.id.english_word)
    lateinit var englishWordEdt : EditText

    @BindView(R.id.error_content)
    lateinit var errorContent : LinearLayout

    @BindView(R.id.error_russian_word)
    lateinit var errorRussianWordTV : TextView

    @BindView(R.id.error_your_english_word)
    lateinit var errorYourEnglishWordTV : TextView

    @BindView(R.id.error_right_english_word)
    lateinit var errorRightInglishWordTV : TextView

    var canSpeak : Boolean = false

    @Inject
    lateinit var presenter : ExamUserPresenter

    @BindView(R.id.resultTV)
    lateinit var resultTV : TextView

    lateinit var tts : TextToSpeech

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view =  LayoutInflater.from(activity)
                .inflate(R.layout.fragment_exam_user, container, false);
        ButterKnife.bind(this, view)
        MyApp.sComponentHolder.examUserComponent.inject(this)
        tts = TextToSpeech(activity, this)
        if(activity is MainActivity) {
            presenter.onCreate(this, activity as MainActivity)
            val fab = (activity as MainActivity).findViewById<FloatingActionButton>(R.id.fab)
            fab.visibility = View.INVISIBLE
        }

        return view
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.onDestroy()
        tts.stop()
        tts.shutdown()
    }

    override fun onDestroy() {
        super.onDestroy()
        if(isRemoving)
            MyApp.sComponentHolder.releaseExamUserComponent()
    }

    @OnClick(R.id.checkButton)
    fun checkButton() {
        presenter.onClickCheck(englishWordEdt.text.toString())
    }

    fun speakWord(word : String) {
        tts.speak(word, TextToSpeech.QUEUE_FLUSH, null)
    }

    fun setWord(russianWord : String) {
        examContent.visibility = View.VISIBLE
        errorContent.visibility = View.INVISIBLE
        russianWordTV.setText(russianWord)
        englishWordEdt.text.clear()
    }

    fun setError(russianWord: String, yourEnglishWord : String, rightEnglishWord : String) {
        examContent.visibility = View.INVISIBLE
        errorContent.visibility = View.VISIBLE
        errorRussianWordTV.setText(russianWord)
        errorYourEnglishWordTV.setText(yourEnglishWord)
        errorRightInglishWordTV.setText(rightEnglishWord)
    }

    fun setResult(result : String) {
        resultTV.visibility = View.VISIBLE
        resultTV.setText(result)
        examContent.visibility = View.INVISIBLE
        errorContent.visibility = View.INVISIBLE
    }

    @OnClick(R.id.error_content)
    fun clickErrorContent() {
        presenter.onClickErrorContent()
    }

    @OnClick(R.id.resultTV)
    fun clickResult() {
        presenter.onClickResult()
    }

}