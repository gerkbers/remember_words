package com.gerkbers.remeberwords.view.edit_words

import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.gerkbers.remeberwords.application.MyApp
import com.gerkbers.remeberwords.MainActivity
import com.gerkbers.remeberwords.presenter.edit_words_presenter.EditWordsPresenter
import com.gerkbers.remeberwords.presenter.WordVTO
import com.gerkbers.remeberwords.R
import javax.inject.Inject

/**
 * Created by k.bersenev on 10.11.2017.
 * com.gerkbers.remeberwords.View.EditWords
 */
class EditWordsFragment : Fragment() {


    @BindView(R.id.edit_words_recycler_view)
    lateinit var recyclerView : RecyclerView

    @BindView(R.id.edit_words_no_words_view)
    lateinit var noWordsView : TextView

    @Inject
    lateinit var presenter : EditWordsPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view  = LayoutInflater.from(activity)
                .inflate(R.layout.fragment_edit_words, container, false)
        ButterKnife.bind(this, view)
        initRecyclerView()
        MyApp.sComponentHolder.wordsComponent.inject(this)
        if(activity is MainActivity) {
            presenter.onCreate(this, activity as MainActivity)
            val fab = (activity as MainActivity).findViewById<FloatingActionButton>(R.id.fab)
            fab.visibility = View.VISIBLE
            fab.setOnClickListener {
                presenter.onClickAddWord()
            }
        }

        return view
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.onDestroy()
    }

    override fun onDestroy() {
        super.onDestroy()
        if(isRemoving)
            MyApp.sComponentHolder.releaseWordsComponent()
    }

    fun initRecyclerView() {
        val layoutManager = LinearLayoutManager(activity)
        recyclerView.layoutManager = layoutManager
        recyclerView.addItemDecoration(DividerItemDecoration(activity, layoutManager.orientation))
    }

    fun setList(list : List<WordVTO>) {
        setVisibleList(true)
        recyclerView.adapter = EditWordsAdapter(activity!!, list)
    }

    fun setEmpty() {
        setVisibleList(false)
    }

    private fun setVisibleList(visible : Boolean) {
        recyclerView.visibility = if(visible) View.VISIBLE else View.INVISIBLE
        noWordsView.visibility = if(visible) View.INVISIBLE else View.VISIBLE
    }
}