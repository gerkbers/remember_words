package com.gerkbers.remeberwords.view.edit_words

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.gerkbers.remeberwords.presenter.WordVTO
import com.gerkbers.remeberwords.R

/**
 * Created by k.bersenev on 10.11.2017.
 * com.gerkbers.remeberwords.View.EditWords
 */
class EditWordsAdapter(var context : Context, val list : List<WordVTO>) : RecyclerView.Adapter<EditWordsHolder>() {


    override fun onBindViewHolder(holder: EditWordsHolder, position: Int) {
        holder!!.bind(list[position])
    }

    override fun getItemCount(): Int = list.size


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EditWordsHolder {
        val view = LayoutInflater.from(context)
                .inflate(R.layout.item_edit_words, parent, false)
        return EditWordsHolder(view)
    }
}