package com.gerkbers.remeberwords.view.edit_words

import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.widget.EditText
import com.gerkbers.remeberwords.application.MyApp
import com.gerkbers.remeberwords.presenter.edit_words_presenter.EditWordsPresenter
import com.gerkbers.remeberwords.R
import javax.inject.Inject

/**
 * Created by k.bersenev on 14.11.2017.
 * com.gerkbers.remeberwords.View.EditWords
 */
class EditWordsDialog : DialogFragment() {

    @Inject
    lateinit var presenter : EditWordsPresenter

    lateinit var englishEdt : EditText
    lateinit var russianEdt : EditText
    lateinit var transcriptionEdt : EditText

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        MyApp.sComponentHolder.wordsComponent.inject(this)

        val view = LayoutInflater.from(context)
                .inflate(R.layout.dialog_add_words, null, false)

        englishEdt = view.findViewById<EditText>(R.id.editWordDialogEnglishWord)

        russianEdt = view.findViewById<EditText>(R.id.editWordDialogRussianWord)

        transcriptionEdt = view.findViewById<EditText>(R.id.editWordDialogTranscription)

        presenter.onStartEditDialog(this)

        return AlertDialog.Builder(context!!)
                .setTitle(R.string.edit_word_dialog_title)
                .setView(view)
                .setPositiveButton(R.string.edit_word_positive_button, { dialog, which ->
                    presenter.onEditWord(englishEdt.text.toString(), russianEdt.text.toString(), transcriptionEdt.text.toString())
                })

                .create()
    }

    fun setWord(english : String, russian : String, transcription : String) {
        englishEdt.setText(english)
        russianEdt.setText(russian)
        transcriptionEdt.setText(transcription)
    }


}