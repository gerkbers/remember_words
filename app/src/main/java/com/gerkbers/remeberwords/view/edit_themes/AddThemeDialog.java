package com.gerkbers.remeberwords.view.edit_themes;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.gerkbers.remeberwords.application.MyApp;
import com.gerkbers.remeberwords.presenter.edit_themes_presenter.EditThemesPresenter;
import com.gerkbers.remeberwords.R;

import javax.inject.Inject;

/**
 * Created by k.bersenev on 09.11.2017.
 * com.gerkbers.remeberwords.View.Main
 */

public class AddThemeDialog extends DialogFragment {

    @Inject
    EditThemesPresenter presenter;



    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        MyApp.getComponentHolder().getEditThemesComponent().inject(this);
        AlertDialog.Builder builder =
                new AlertDialog.Builder(getContext());


        View view = LayoutInflater.from(getContext())
                .inflate(R.layout.dialog_add_theme, null, false);

        EditText editText = view.findViewById(R.id.dialog_edit_theme);

        builder.setTitle(R.string.add_theme_title)
                .setView(view)
                .setPositiveButton(R.string.addTheme, (dialog, which) -> {
                    if(presenter != null) {
                        presenter.onAddTheme(editText.getText().toString());
                    }
                })
                .setNegativeButton(R.string.cancelAddTheme, null);


        return builder.create();
    }
}
