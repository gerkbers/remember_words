package com.gerkbers.remeberwords.view.main

import android.support.v4.app.Fragment
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.gerkbers.remeberwords.application.MyApp
import com.gerkbers.remeberwords.application.MyApp.getComponentHolder
import com.gerkbers.remeberwords.MainActivity
import com.gerkbers.remeberwords.presenter.main_presenter.MainPresenter
import com.gerkbers.remeberwords.R
import com.gerkbers.remeberwords.Router
import javax.inject.Inject

/**
 * A placeholder fragment containing a simple view.
 */
class MainActivityFragment : Fragment() {

    @Inject
    lateinit var presenter : MainPresenter

    @BindView(R.id.mainFragmentRecyclerView)
    lateinit var recyclerView : RecyclerView

    @BindView(R.id.no_categories_title)
    lateinit var noCategoriesView : TextView

    lateinit var fab : FloatingActionButton

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_main, container, false)
        getComponentHolder().checkUserComponent.inject(this)
        ButterKnife.bind(this, view);
        initRecyclerView()

        if(activity is Router){
            presenter.onCreate(this, activity as Router)
        }
        if(activity is MainActivity) {
            (activity as MainActivity).findViewById<FloatingActionButton>(R.id.fab).visibility = View.INVISIBLE
            fab = (activity as MainActivity).findViewById<FloatingActionButton>(R.id.startFab)
            fab.visibility = View.VISIBLE
            fab.setOnClickListener {
                presenter.onClickStartExam()
            }
        }
        return view
    }

    override fun onDestroyView() {
        super.onDestroyView()
        fab.visibility = View.INVISIBLE
        presenter.onDestroy()
    }

    override fun onDestroy() {
        super.onDestroy()
        if(isRemoving)
            MyApp.sComponentHolder.releaseCheckUserComponent()
    }

    fun initRecyclerView() {
        val layoutManager = LinearLayoutManager(activity)
        recyclerView.layoutManager = layoutManager
        recyclerView.addItemDecoration(DividerItemDecoration(activity, layoutManager.orientation))
    }

    fun setList(list : List<CategoryVTO>) {
        recyclerView.visibility = View.VISIBLE
        noCategoriesView.visibility = View.INVISIBLE
        recyclerView.adapter = MainAdapter(activity, list)
    }

    fun setEmpty() {
        recyclerView.visibility = View.INVISIBLE
        noCategoriesView.visibility = View.VISIBLE
    }


}
