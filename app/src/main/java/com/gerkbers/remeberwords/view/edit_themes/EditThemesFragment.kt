package com.gerkbers.remeberwords.view.edit_themes

import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.gerkbers.remeberwords.application.MyApp
import com.gerkbers.remeberwords.MainActivity
import com.gerkbers.remeberwords.presenter.edit_themes_presenter.EditThemesPresenter
import com.gerkbers.remeberwords.R
import com.gerkbers.remeberwords.view.main.CategoryVTO
import javax.inject.Inject

/**
 * Created by k.bersenev on 10.11.2017.
 * com.gerkbers.remeberwords.View.EditThemes
 */
class EditThemesFragment : Fragment() {

    @BindView(R.id.edit_themes_recycler_view)
    lateinit var themesView : RecyclerView

    @BindView(R.id.edit_themes_no_themes_view)
    lateinit var noThemesView : TextView

    @Inject
    lateinit var presenter : EditThemesPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = LayoutInflater.from(activity)
                .inflate(R.layout.fragment_edit_themes, container, false)
        ButterKnife.bind(this, view)
        MyApp.sComponentHolder.editThemesComponent.inject(this)
        initRecyclerView()

        if(activity is MainActivity) {
            presenter.onCreate(this, activity as MainActivity)
            val fab = (activity as MainActivity).findViewById<FloatingActionButton>(R.id.fab)
            fab.visibility = View.VISIBLE
            fab.setOnClickListener {
                presenter.onClickAddTheme()
            }
        }
        return view
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.onDestroy()
    }

    override fun onDestroy() {
        super.onDestroy()
        if(isRemoving) {
            MyApp.sComponentHolder.releaseEditThemesComponent()
        }
    }

    private fun initRecyclerView() {
        val layoutManager = LinearLayoutManager(context)
        themesView.layoutManager = layoutManager
        themesView.addItemDecoration(DividerItemDecoration(context, layoutManager.orientation))
    }

    fun setList(list : List<CategoryVTO>) {
        setVisibleList(true)
        themesView.adapter = EditThemesAdapter(context!!, list)
    }

    fun setEmpty() {
        setVisibleList(false)
    }

    private fun setVisibleList(visible : Boolean) {
        themesView.visibility = if(visible) View.VISIBLE else View.INVISIBLE
        noThemesView.visibility = if(visible) View.INVISIBLE else View.VISIBLE
    }
}