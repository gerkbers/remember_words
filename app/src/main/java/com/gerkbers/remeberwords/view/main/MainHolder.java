package com.gerkbers.remeberwords.view.main;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import com.gerkbers.remeberwords.application.MyApp;
import com.gerkbers.remeberwords.presenter.main_presenter.MainPresenter;
import com.gerkbers.remeberwords.R;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by k.bersenev on 09.11.2017.
 * com.gerkbers.remeberwords.View.Main
 */

public class MainHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.mainItemCheckbox)
    CheckBox mCheckBox;
    @BindView(R.id.mainItemName)
    TextView mNameView;

    @Inject
    MainPresenter presenter;

    CategoryVTO mCategoryVTO;

    long id;

    public MainHolder(View itemView) {
        super(itemView);
        MyApp.getComponentHolder().getCheckUserComponent().inject(this);
        ButterKnife.bind(this, itemView);
        mCheckBox.setOnCheckedChangeListener((buttonView, isChecked) -> {
            mCategoryVTO.setChecked(isChecked);
            presenter.onChangeChecked(mCategoryVTO);
        });
    }

    public void bind(CategoryVTO category) {
        mCategoryVTO = category;
        id =  category.getId();
        mCheckBox.setChecked(category.isChecked());
        mNameView.setText(category.getName());
    }
}
