package com.gerkbers.remeberwords.view.edit_themes

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.gerkbers.remeberwords.R
import com.gerkbers.remeberwords.view.main.CategoryVTO

/**
 * Created by k.bersenev on 10.11.2017.
 * com.gerkbers.remeberwords.View.EditThemes
 */

class EditThemesAdapter(val context : Context, var list : List<CategoryVTO>) : RecyclerView.Adapter<EditThemesItem>() {


    override fun onBindViewHolder(holder: EditThemesItem, position: Int) {
        holder.bind(list.get(position))
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EditThemesItem {
        val view = LayoutInflater.from(context)
                .inflate(R.layout.item_edit_themes, parent, false);
        return EditThemesItem(view)
    }

    override fun getItemCount(): Int = list.size
}
