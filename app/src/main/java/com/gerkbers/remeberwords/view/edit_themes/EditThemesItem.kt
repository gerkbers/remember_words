package com.gerkbers.remeberwords.view.edit_themes

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.gerkbers.remeberwords.application.MyApp
import com.gerkbers.remeberwords.presenter.edit_themes_presenter.EditThemesPresenter
import com.gerkbers.remeberwords.R
import com.gerkbers.remeberwords.view.main.CategoryVTO
import javax.inject.Inject

/**
 * Created by k.bersenev on 10.11.2017.
 * com.gerkbers.remeberwords.View.EditThemes
 */
class EditThemesItem(view : View) : RecyclerView.ViewHolder(view){

    @BindView(R.id.editThemesItemName)
    lateinit var name : TextView
    @BindView(R.id.EditThemesItemDelete)
    lateinit var deleteBtn : ImageView

    @Inject
    lateinit var presenter : EditThemesPresenter

    var id : Long = 0

    init {
        MyApp.sComponentHolder.editThemesComponent.inject(this)
        ButterKnife.bind(this, view)

        view.setOnClickListener {
            presenter.onClickTheme(id)
        }

        deleteBtn.setOnClickListener {
            presenter.onClickDelete(id)
        }
    }

    fun bind(theme : CategoryVTO) {
        id = theme.id
        name.setText(theme.name)
    }
}