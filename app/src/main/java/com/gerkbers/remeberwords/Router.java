package com.gerkbers.remeberwords;

/**
 * Created by k.bersenev on 09.11.2017.
 * com.gerkbers.remeberwords
 */

public interface Router {

    void showAddThemeDialog();

    void showExam();

    void showThemeWords(long themeID);
    long getThemeId();

    void showAddWordDialog();

    void backToMain();

    void showEditWordDialog();

    void setTitleHeader(int titleRes);

    void setTitleHeader(String title);
}
