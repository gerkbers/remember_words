package com.gerkbers.remeberwords.presenter.exam_user_presenter

import com.gerkbers.remeberwords.application.MyApp
import com.gerkbers.remeberwords.model.entities.Word
import com.gerkbers.remeberwords.model.Model
import com.gerkbers.remeberwords.presenter.BasePresenter
import com.gerkbers.remeberwords.R
import com.gerkbers.remeberwords.view.exam_user.ExamUserFragment
import java.util.*
import javax.inject.Inject

/**
 * Created by k.bersenev on 13.11.2017.
 * com.gerkbers.remeberwords.Presenter.ExamUserPresenter
 */
class ExamUserPresenter(model : Model) : BasePresenter<ExamUserFragment>(model) {

    var list : List<WordExamVTO> = ArrayList()

    @Inject
    lateinit var map : WordExamMap

    var current : Int = 0

    var countError : Int = 0

    override fun onCreate() {
        router.setTitleHeader(R.string.exam_title)
        MyApp.sComponentHolder.appComponent.inject(this)
        loadList();
    }

    override fun onDestr() {

    }

    private fun loadList() {
        if(current != 0) {
            showNextWord()
        }
        else {
            model.themes
                    .flatMap { themes -> model.getExamWords(30, themes) }
                    .map { list -> map.apply(list) }
                    .subscribe(this::onLoadList);
        }

    }

    private fun onLoadList(list : List<WordExamVTO>) {
        this.list = list
        Collections.shuffle(this.list, Random(System.nanoTime()))
        showNextWord()
    }

    private fun showNextWord() {
        if(current < list.size) {
            view.setWord(list[current].russian)
        }
        else {
            view.setResult(countError.toString() + "/" + list.size + " ошибок")
        }
    }

    fun onClickCheck(yourEnglish : String) {
        val currentItem = list[current]
        val yourAnswer = yourEnglish.trim().toLowerCase()
        val rightAnswer = currentItem.english.trim().toLowerCase()
        if(yourAnswer.equals(rightAnswer)) {
            val word = Word(
                    currentItem.id,
                    currentItem.english,
                    currentItem.russian,
                    currentItem.transcription,
                    currentItem.themeId,
                    System.currentTimeMillis()
            )
            model.updateWord(word)
                    .subscribe()
            view.speakWord(currentItem.english)
            current ++
            showNextWord()
        }
        else {
            view.setError(currentItem.russian, yourEnglish, currentItem.english)
            view.speakWord(currentItem.english)
            countError ++
        }
    }

    fun onClickErrorContent() {
        current ++
        showNextWord()
    }

    fun onClickResult() {
        router.backToMain()
    }

}