package com.gerkbers.remeberwords.presenter.exam_user_presenter.dagger

import com.gerkbers.remeberwords.view.exam_user.ExamUserFragment
import dagger.Subcomponent

/**
 * Created by k.bersenev on 13.11.2017.
 * com.gerkbers.remeberwords.Presenter.ExamUserPresenter.dagger
 */
@ExamUserScope
@Subcomponent(modules = arrayOf(ExamUserModule::class))
interface ExamUserComponent {
    fun inject(examUserFragment: ExamUserFragment)


}