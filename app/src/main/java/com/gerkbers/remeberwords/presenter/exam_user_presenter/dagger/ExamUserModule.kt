package com.gerkbers.remeberwords.presenter.exam_user_presenter.dagger

import com.gerkbers.remeberwords.model.Model
import com.gerkbers.remeberwords.presenter.exam_user_presenter.ExamUserPresenter
import dagger.Module
import dagger.Provides

/**
 * Created by k.bersenev on 13.11.2017.
 * com.gerkbers.remeberwords.Presenter.ExamUserPresenter.dagger
 */
@Module
class ExamUserModule {

    @ExamUserScope
    @Provides
    fun providePresenter(model : Model) : ExamUserPresenter {
        return ExamUserPresenter(model)
    }
}