package com.gerkbers.remeberwords.presenter.exam_user_presenter

import com.gerkbers.remeberwords.model.entities.Word
import io.reactivex.Observable
import io.reactivex.functions.Function
import javax.inject.Inject

/**
 * Created by k.bersenev on 13.11.2017.
 * com.gerkbers.remeberwords.Presenter.ExamUserPresenter
 */
class WordExamMap @Inject constructor(): Function<List<Word>, List<WordExamVTO>> {
    override fun apply(t: List<Word>): List<WordExamVTO> {
        return Observable.fromIterable(t)
                .map { word: Word -> WordExamVTO(word.id, word.english, word.russian, word.theme, word.transcription) }
                .toList()
                .blockingGet();
    }

}