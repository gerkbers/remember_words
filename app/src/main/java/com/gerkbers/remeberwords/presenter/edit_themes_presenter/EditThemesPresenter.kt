package com.gerkbers.remeberwords.presenter.edit_themes_presenter

import com.gerkbers.remeberwords.application.MyApp
import com.gerkbers.remeberwords.model.Model
import com.gerkbers.remeberwords.presenter.BasePresenter
import com.gerkbers.remeberwords.presenter.main_presenter.ThemesMapper
import com.gerkbers.remeberwords.R
import com.gerkbers.remeberwords.view.edit_themes.EditThemesFragment
import javax.inject.Inject

/**
 * Created by k.bersenev on 10.11.2017.
 * com.gerkbers.remeberwords.Presenter.EditThemesPresenter
 */
class EditThemesPresenter(model : Model) : BasePresenter<EditThemesFragment>(model){

    @Inject
    lateinit var mapper : ThemesMapper

    override fun onCreate() {
        router.setTitleHeader(R.string.dictionaryTitle)
        MyApp.sComponentHolder.appComponent.inject(this)
        loadThemes()
    }

    override fun onDestr() {

    }

    fun loadThemes() {
        model.themes
                .map(mapper)
                .subscribe({ list ->
                    if(isAttachView) {
                        if(list.size == 0) {
                            view.setEmpty()
                        }
                        else {
                            view.setList(list)
                        }
                    }
                })
    }

    fun onClickAddTheme() {
        router.showAddThemeDialog()
    }

    fun onAddTheme(theme : String) {
        model.addTheme(theme)
                .subscribe({loadThemes()})
    }

    fun onClickDelete(id : Long) {
        model.removeTheme(id)
                .subscribe({
                    loadThemes()
                })
    }

    fun onClickTheme(id: Long) {
        router.showThemeWords(id)
    }
}