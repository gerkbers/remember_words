package com.gerkbers.remeberwords.presenter.main_presenter.dagger;

import com.gerkbers.remeberwords.model.Model;
import com.gerkbers.remeberwords.presenter.main_presenter.MainPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by k.bersenev on 09.11.2017.
 * com.gerkbers.remeberwords.Presenter.MainPresenter.dagger
 */
@Module
public class CheckUserModule {

    @Provides
    @CheckUserScope
    MainPresenter provide(Model model) {
        return new MainPresenter(model);
    }
}
