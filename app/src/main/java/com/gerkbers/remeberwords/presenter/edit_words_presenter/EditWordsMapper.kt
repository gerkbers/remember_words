package com.gerkbers.remeberwords.presenter.edit_words_presenter

import com.gerkbers.remeberwords.model.entities.Word
import com.gerkbers.remeberwords.presenter.WordVTO
import io.reactivex.Observable
import io.reactivex.functions.Function
import javax.inject.Inject

/**
 * Created by k.bersenev on 10.11.2017.
 * com.gerkbers.remeberwords.Presenter.EditWordsPresenter
 */
class EditWordsMapper @Inject constructor(): Function<List<Word>, List<WordVTO>> {
    override fun apply(t: List<Word>): List<WordVTO> {
        return Observable.fromIterable(t)
                .map { word -> WordVTO(word.id, word.english + " - " + word.russian) }
                .toList()
                .blockingGet()
    }
}