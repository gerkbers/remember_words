package com.gerkbers.remeberwords.presenter.main_presenter.dagger;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by k.bersenev on 09.11.2017.
 * com.gerkbers.remeberwords.Presenter.MainPresenter.dagger
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface CheckUserScope {
}
