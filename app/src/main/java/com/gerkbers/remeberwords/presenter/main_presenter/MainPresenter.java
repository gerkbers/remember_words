package com.gerkbers.remeberwords.presenter.main_presenter;

import com.gerkbers.remeberwords.application.MyApp;
import com.gerkbers.remeberwords.model.entities.Theme;
import com.gerkbers.remeberwords.model.Model;
import com.gerkbers.remeberwords.presenter.BasePresenter;
import com.gerkbers.remeberwords.R;
import com.gerkbers.remeberwords.view.main.CategoryVTO;
import com.gerkbers.remeberwords.view.main.MainActivityFragment;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by k.bersenev on 09.11.2017.
 * com.gerkbers.remeberwords.Presenter
 */

public class MainPresenter extends BasePresenter<MainActivityFragment> {

    @Inject
    ThemesMapper themesMapper;

    public MainPresenter(Model model) {
        super(model);
        MyApp.getComponentHolder().getAppComponent().inject(this);
    }

    @Override
    public void onCreate() {
        getRouter().setTitleHeader(R.string.choiceCategory);
        loadListCategories();
        getModel().addThemeListener(listener);
    }

    @Override
    public void onDestr() {
        getModel().removeThemeListener(listener);
    }

    public void loadListCategories() {
        getModel().getThemes()
                .map(themesMapper)
                .subscribe(this::onLoadCategories);
    }

    private void onLoadCategories(List<CategoryVTO> categories) {
        if(isAttachView()) {
            if(categories.size() == 0) {
                getView().setEmpty();
            }
            else {
                getView().setList(categories);
            }
        }
    }

    public void onChangeChecked(CategoryVTO categoryVTO) {
        Theme theme = new Theme(categoryVTO.getId(), categoryVTO.getName(), categoryVTO.isChecked());
        getModel().updateTheme(theme)
                .subscribe();
    }

    private Model.OnChangeThemesListener listener = this::loadListCategories;

    public void onClickStartExam() {
        getRouter().showExam();
    }
}
