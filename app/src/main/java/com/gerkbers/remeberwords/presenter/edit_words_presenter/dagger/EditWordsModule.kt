package com.gerkbers.remeberwords.presenter.edit_words_presenter.dagger

import com.gerkbers.remeberwords.model.Model
import com.gerkbers.remeberwords.presenter.edit_words_presenter.EditWordsPresenter
import dagger.Module
import dagger.Provides

/**
 * Created by k.bersenev on 10.11.2017.
 * com.gerkbers.remeberwords.Presenter.EditWordsPresenter.dagger
 */
@Module
class EditWordsModule {

    @Provides
    @EditWordsScope
    fun providePresenter(model : Model) : EditWordsPresenter = EditWordsPresenter(model)
}