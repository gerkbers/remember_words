package com.gerkbers.remeberwords.presenter.main_presenter;

import com.gerkbers.remeberwords.model.entities.Theme;
import com.gerkbers.remeberwords.view.main.CategoryVTO;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;

/**
 * Created by k.bersenev on 09.11.2017.
 * com.gerkbers.remeberwords.Presenter.MainPresenter
 */

public class ThemesMapper implements Function<List<Theme>, List<CategoryVTO>> {

    @Inject
    public ThemesMapper() {
    }

    @Override
    public List<CategoryVTO> apply(@NonNull List<Theme> themes) throws Exception {
        if(themes == null) return null;

        return Observable.fromIterable(themes)
                .map(theme -> new CategoryVTO(theme.getId(), theme.getName(), theme.getIsChecked()))
                .toList()
                .blockingGet();
    }
}
