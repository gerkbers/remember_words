package com.gerkbers.remeberwords.presenter.edit_words_presenter

import com.gerkbers.remeberwords.application.MyApp
import com.gerkbers.remeberwords.model.entities.Theme
import com.gerkbers.remeberwords.model.entities.Word
import com.gerkbers.remeberwords.model.Model
import com.gerkbers.remeberwords.presenter.BasePresenter
import com.gerkbers.remeberwords.view.edit_words.EditWordsDialog
import com.gerkbers.remeberwords.view.edit_words.EditWordsFragment
import javax.inject.Inject

/**
 * Created by k.bersenev on 10.11.2017.
 * com.gerkbers.remeberwords.Presenter.EditWordsPresenter
 */
class EditWordsPresenter(model : Model) : BasePresenter<EditWordsFragment>(model) {

    @Inject
    lateinit var mapper : EditWordsMapper

    lateinit var  theme : Theme

    lateinit var editableWord : Word

    override fun onCreate() {
        MyApp.sComponentHolder.appComponent.inject(this)
        loadWords()
    }

    override fun onDestr() {

    }

    private fun loadWords() {
        model.getThemeById(router.themeId)
                .map{ theme ->
                    this.theme  = theme
                    mapper.apply(theme.words)
                }
                .subscribe({ list ->
                    if(isAttachView) {
                        router.setTitleHeader(theme.name)
                        if(list.size == 0) {
                            view.setEmpty()
                        }
                        else {
                            view.setList(list)
                        }
                    }
                })
    }

    fun onClickAddWord() {
        router.showAddWordDialog()
    }

    fun onAddWord(english : String, russian : String, transcription : String) {
        val word = Word()
        word.russian = russian
        word.english = english
        word.transcription = transcription
        word.theme = router.themeId
        theme.words.add(word)
        model.addWord(word)
                .subscribe({loadWords()})
    }

    fun onClickWord(id : Long) {
        editableWord = getWordById(id)
        router.showEditWordDialog()
    }

    fun onStartEditDialog(dialog : EditWordsDialog) {
        dialog.setWord(editableWord.english, editableWord.russian, editableWord.transcription)
    }

    fun onEditWord(english : String, russian : String, transcription : String) {
        editableWord.english = english
        editableWord.russian = russian
        editableWord.transcription = transcription
        model.updateWord(editableWord)
                .subscribe({loadWords()})
    }

    fun onRemoveWord(id : Long) {
        model.removeWord(id)
                .subscribe({loadWords()})
    }

    fun getWordById(id : Long) : Word {
        for(word in theme.words) {
            if(word.id == id) {
                return word
            }
        }
        return Word()
    }
}