package com.gerkbers.remeberwords.presenter.exam_user_presenter.dagger

import javax.inject.Scope

/**
 * Created by k.bersenev on 13.11.2017.
 * com.gerkbers.remeberwords.Presenter.ExamUserPresenter.dagger
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ExamUserScope