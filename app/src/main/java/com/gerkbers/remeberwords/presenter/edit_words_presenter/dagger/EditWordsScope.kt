package com.gerkbers.remeberwords.presenter.edit_words_presenter.dagger

import javax.inject.Scope

/**
 * Created by k.bersenev on 10.11.2017.
 * com.gerkbers.remeberwords.Presenter.EditWordsPresenter.dagger
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class EditWordsScope