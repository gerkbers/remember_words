package com.gerkbers.remeberwords.presenter.exam_user_presenter

/**
 * Created by k.bersenev on 13.11.2017.
 * com.gerkbers.remeberwords.Presenter.ExamUserPresenter
 */
class WordExamVTO(var id : Long, var english : String, var russian : String, var themeId : Long, var transcription : String)