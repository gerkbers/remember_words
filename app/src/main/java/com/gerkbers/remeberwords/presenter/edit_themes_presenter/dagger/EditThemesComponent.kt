package com.gerkbers.remeberwords.presenter.edit_themes_presenter.dagger

import com.gerkbers.remeberwords.view.edit_themes.EditThemesFragment
import com.gerkbers.remeberwords.view.edit_themes.EditThemesItem
import com.gerkbers.remeberwords.view.edit_themes.AddThemeDialog
import dagger.Subcomponent

/**
 * Created by k.bersenev on 10.11.2017.
 * com.gerkbers.remeberwords.Presenter.EditThemesPresenter.dagger
 */
@EditThemesScope
@Subcomponent(modules = arrayOf(EditThemesModule::class))
interface EditThemesComponent {
    fun inject(editThemesFragment: EditThemesFragment)
    fun inject(editThemesFragment: EditThemesItem)
    fun inject(dialog : AddThemeDialog)
}