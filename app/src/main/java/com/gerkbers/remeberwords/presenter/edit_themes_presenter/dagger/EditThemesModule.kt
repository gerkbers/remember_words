package com.gerkbers.remeberwords.presenter.edit_themes_presenter.dagger

import com.gerkbers.remeberwords.model.Model
import com.gerkbers.remeberwords.presenter.edit_themes_presenter.EditThemesPresenter
import dagger.Module
import dagger.Provides

/**
 * Created by k.bersenev on 10.11.2017.
 * com.gerkbers.remeberwords.Presenter.EditThemesPresenter.dagger
 */
@Module
class EditThemesModule {

    @Provides
    @EditThemesScope
    fun providePresenter(model : Model) : EditThemesPresenter = EditThemesPresenter(model)
}