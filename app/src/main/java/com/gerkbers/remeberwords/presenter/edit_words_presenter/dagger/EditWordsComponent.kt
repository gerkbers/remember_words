package com.gerkbers.remeberwords.presenter.edit_words_presenter.dagger

import com.gerkbers.remeberwords.view.edit_words.AddWordsDialog
import com.gerkbers.remeberwords.view.edit_words.EditWordsDialog
import com.gerkbers.remeberwords.view.edit_words.EditWordsFragment
import com.gerkbers.remeberwords.view.edit_words.EditWordsHolder
import dagger.Subcomponent

/**
 * Created by k.bersenev on 10.11.2017.
 * com.gerkbers.remeberwords.Presenter.EditWordsPresenter.dagger
 */
@EditWordsScope
@Subcomponent(modules = arrayOf(EditWordsModule::class))
interface EditWordsComponent {
    fun inject(editWordsHolder: EditWordsHolder)
    fun inject(editWordsHolder: EditWordsFragment)
    fun inject(editWordsDialog: AddWordsDialog)
    fun inject(editWordsDialog: EditWordsDialog)
}