package com.gerkbers.remeberwords.presenter.edit_themes_presenter.dagger;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by k.bersenev on 10.11.2017.
 * com.gerkbers.remeberwords.Presenter.EditThemesPresenter.dagger
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface EditThemesScope {
}
