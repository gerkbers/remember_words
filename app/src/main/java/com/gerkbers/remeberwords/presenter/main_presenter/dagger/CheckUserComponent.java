package com.gerkbers.remeberwords.presenter.main_presenter.dagger;

import com.gerkbers.remeberwords.view.main.MainActivityFragment;
import com.gerkbers.remeberwords.view.main.MainHolder;

import org.jetbrains.annotations.NotNull;

import dagger.Subcomponent;

/**
 * Created by k.bersenev on 09.11.2017.
 * com.gerkbers.remeberwords.Presenter.MainPresenter.dagger
 */
@CheckUserScope
@Subcomponent(modules = {CheckUserModule.class})
public interface CheckUserComponent {
    void inject(@NotNull MainActivityFragment mainActivityFragment);

    void inject(MainHolder mainHolder);
}
