package com.gerkbers.remeberwords.presenter;

import com.gerkbers.remeberwords.model.Model;
import com.gerkbers.remeberwords.Router;

/**
 * Created by k.bersenev on 09.11.2017.
 * com.gerkbers.remeberwords.Presenter
 */

public abstract class BasePresenter<V> {

    Model mModel;
    V view;
    Router mRouter;

    public abstract void onCreate();
    public abstract void onDestr();

    public BasePresenter(Model model) {
        mModel = model;
    }

    public void onCreate(V view, Router router) {
        this.view = view;
        this.mRouter = router;
        onCreate();
    }

    public void onDestroy() {
        view = null;
        onDestr();
    }

    public boolean isAttachView() {
        return view != null;
    }

    public Model getModel() {
        return mModel;
    }

    public V getView() {
        return view;
    }

    public Router getRouter() {
        return mRouter;
    }
}
