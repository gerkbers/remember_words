package com.gerkbers.remeberwords;

import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by k.bersenev on 09.11.2017.
 * com.gerkbers.remeberwords
 */

public class Utils {

    public static <T> Observable<T> createObservable(ObservableOnSubscribe<T> observableOnSubscribe) {
        return Observable.create(observableOnSubscribe)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());
    }
}
