package com.gerkbers.remeberwords.application.dagger;

import com.gerkbers.remeberwords.model.dagger.ModelComponent;
import com.gerkbers.remeberwords.model.dagger.ModelModule;
import com.gerkbers.remeberwords.presenter.exam_user_presenter.ExamUserPresenter;
import com.gerkbers.remeberwords.presenter.main_presenter.MainPresenter;
import com.gerkbers.remeberwords.presenter.edit_themes_presenter.EditThemesPresenter;
import com.gerkbers.remeberwords.presenter.edit_words_presenter.EditWordsPresenter;

import org.jetbrains.annotations.NotNull;

import dagger.Component;

/**
 * Created by k.bersenev on 09.11.2017.
 * com.gerkbers.remeberwords.Application.dagger
 */
@AppScope
@Component(modules = {AppModule.class})
public interface AppComponent {

    ModelComponent plus(ModelModule modelModule);

    void inject(MainPresenter checkUserPresenter);

    void inject(EditThemesPresenter editThemesPresenter);

    void inject(@NotNull EditWordsPresenter editWordsPresenter);

    void inject(@NotNull ExamUserPresenter examUserPresenter);
}
