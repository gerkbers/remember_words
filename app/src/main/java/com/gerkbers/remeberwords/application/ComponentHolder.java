package com.gerkbers.remeberwords.application;

import android.content.Context;

import com.gerkbers.remeberwords.application.dagger.AppComponent;
import com.gerkbers.remeberwords.application.dagger.AppModule;
import com.gerkbers.remeberwords.application.dagger.DaggerAppComponent;
import com.gerkbers.remeberwords.model.dagger.ModelComponent;
import com.gerkbers.remeberwords.model.dagger.ModelModule;
import com.gerkbers.remeberwords.presenter.exam_user_presenter.dagger.ExamUserComponent;
import com.gerkbers.remeberwords.presenter.exam_user_presenter.dagger.ExamUserModule;
import com.gerkbers.remeberwords.presenter.main_presenter.dagger.CheckUserComponent;
import com.gerkbers.remeberwords.presenter.main_presenter.dagger.CheckUserModule;
import com.gerkbers.remeberwords.presenter.edit_themes_presenter.dagger.EditThemesComponent;
import com.gerkbers.remeberwords.presenter.edit_themes_presenter.dagger.EditThemesModule;
import com.gerkbers.remeberwords.presenter.edit_words_presenter.dagger.EditWordsComponent;
import com.gerkbers.remeberwords.presenter.edit_words_presenter.dagger.EditWordsModule;

/**
 * Created by k.bersenev on 09.11.2017.
 * com.gerkbers.remeberwords.Application
 */

public class ComponentHolder {

    private Context mContext;

    private AppComponent mAppComponent;

    private ModelComponent mModelComponent;

    private CheckUserComponent mCheckUserComponent;

    private EditThemesComponent mEditThemesComponent;

    private EditWordsComponent mWordsComponent;

    private ExamUserComponent mExamUserComponent;


    public ComponentHolder(Context context) {
        mAppComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(context))
                .build();
    }

    public AppComponent getAppComponent() {
        return mAppComponent;
    }

    public ModelComponent getModelComponent(){
        if(mModelComponent == null) {
            mModelComponent = mAppComponent.plus(new ModelModule());
        }
        return mModelComponent;
    }

    public CheckUserComponent getCheckUserComponent() {
        if(mCheckUserComponent == null) {
            mCheckUserComponent = getModelComponent().plus(new CheckUserModule());
        }
        return mCheckUserComponent;
    }

    public void releaseCheckUserComponent() {
        mCheckUserComponent = null;
    }

    public EditThemesComponent getEditThemesComponent() {
        if(mEditThemesComponent == null) {
            mEditThemesComponent = getModelComponent().plus(new EditThemesModule());
        }
        return mEditThemesComponent;
    }

    public void releaseEditThemesComponent() {
        mEditThemesComponent = null;
    }

    public EditWordsComponent getWordsComponent() {
        if(mWordsComponent == null) {
            mWordsComponent = getModelComponent().plus(new EditWordsModule());
        }
        return mWordsComponent;
    }

    public void releaseWordsComponent() {
        mWordsComponent = null;
    }

    public ExamUserComponent getExamUserComponent() {
        if(mExamUserComponent == null) {
            mExamUserComponent = getModelComponent().plus(new ExamUserModule());
        }
        return mExamUserComponent;
    }

    public void releaseExamUserComponent() {
        mExamUserComponent = null;
    }
}
