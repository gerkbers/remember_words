package com.gerkbers.remeberwords.application;

import android.app.Application;

/**
 * Created by k.bersenev on 09.11.2017.
 * com.gerkbers.remeberwords
 */

public class MyApp extends Application {

    public static ComponentHolder sComponentHolder;

    @Override
    public void onCreate() {
        super.onCreate();
        sComponentHolder = new ComponentHolder(this);
    }

    public static ComponentHolder getComponentHolder() {
        return sComponentHolder;
    }
}
