package com.gerkbers.remeberwords.application.dagger;

import android.content.Context;

import com.gerkbers.remeberwords.model.entities.DaoMaster;
import com.gerkbers.remeberwords.model.entities.DaoSession;

import org.greenrobot.greendao.database.Database;

import dagger.Module;
import dagger.Provides;

/**
 * Created by k.bersenev on 09.11.2017.
 * com.gerkbers.remeberwords.Application.dagger
 */
@Module
public class AppModule {

    DaoSession mDaoSession;

    public AppModule(Context context) {
        initDaoSession(context.getApplicationContext());
    }

    private void initDaoSession(Context context) {
        DaoMaster.DevOpenHelper helper =
                new DaoMaster.DevOpenHelper(context, "RememberWords");
        Database db = helper.getWritableDb();
        mDaoSession = new DaoMaster(db).newSession();
    }

    @AppScope
    @Provides
    public DaoSession provideDaoSession(){
        return mDaoSession;
    }
}
