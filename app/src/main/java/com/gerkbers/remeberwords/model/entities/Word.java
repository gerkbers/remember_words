package com.gerkbers.remeberwords.model.entities;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Unique;

/**
 * Created by k.bersenev on 09.11.2017.
 * com.gerkbers.remeberwords.Model
 */
@Entity
public class Word {

    @Unique
    @Id(autoincrement = true)
    private Long id;
    private String mEnglish;
    private String mRussian;
    private String mTranscription;
    private long mTheme;
    private long mTimeLastRepeat;



    @Generated(hash = 1209165368)
    public Word(Long id, String mEnglish, String mRussian, String mTranscription,
            long mTheme, long mTimeLastRepeat) {
        this.id = id;
        this.mEnglish = mEnglish;
        this.mRussian = mRussian;
        this.mTranscription = mTranscription;
        this.mTheme = mTheme;
        this.mTimeLastRepeat = mTimeLastRepeat;
    }

    public Word(long id, String english, String russian, String transcription, long theme, long timeLastRepeat) {
        this.id = id;
        mEnglish = english;
        mRussian = russian;
        mTranscription = transcription;
        mTheme = theme;
        mTimeLastRepeat = timeLastRepeat;
    }

    @Generated(hash = 3342184)
    public Word() {
    }

    

    public String getEnglish() {
        return mEnglish;
    }

    public void setEnglish(String english) {
        mEnglish = english;
    }

    public String getRussian() {
        return mRussian;
    }

    public void setRussian(String russian) {
        mRussian = russian;
    }

    public String getTranscription() {
        return mTranscription;
    }

    public void setTranscription(String transcription) {
        mTranscription = transcription;
    }

    public long getTimeLastRepeat() {
        return mTimeLastRepeat;
    }

    public void setTimeLastRepeat(long timeLastRepeat) {
        mTimeLastRepeat = timeLastRepeat;
    }

    public long getTheme() {
        return mTheme;
    }

    public void setTheme(long theme) {
        mTheme = theme;
    }

    public String getMEnglish() {
        return this.mEnglish;
    }

    public void setMEnglish(String mEnglish) {
        this.mEnglish = mEnglish;
    }

    public String getMRussian() {
        return this.mRussian;
    }

    public void setMRussian(String mRussian) {
        this.mRussian = mRussian;
    }

    public String getMTranscription() {
        return this.mTranscription;
    }

    public void setMTranscription(String mTranscription) {
        this.mTranscription = mTranscription;
    }

    public long getMTheme() {
        return this.mTheme;
    }

    public void setMTheme(long mTheme) {
        this.mTheme = mTheme;
    }

    public long getMTimeLastRepeat() {
        return this.mTimeLastRepeat;
    }

    public void setMTimeLastRepeat(long mTimeLastRepeat) {
        this.mTimeLastRepeat = mTimeLastRepeat;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Word word = (Word) o;

        if (mTheme != word.mTheme) return false;
        if (id != null ? !id.equals(word.id) : word.id != null) return false;
        if (mEnglish != null ? !mEnglish.equals(word.mEnglish) : word.mEnglish != null)
            return false;
        if (mRussian != null ? !mRussian.equals(word.mRussian) : word.mRussian != null)
            return false;
        return mTranscription != null ? mTranscription.equals(word.mTranscription) : word.mTranscription == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (mEnglish != null ? mEnglish.hashCode() : 0);
        result = 31 * result + (mRussian != null ? mRussian.hashCode() : 0);
        result = 31 * result + (mTranscription != null ? mTranscription.hashCode() : 0);
        result = 31 * result + (int) (mTheme ^ (mTheme >>> 32));
        return result;
    }
}
