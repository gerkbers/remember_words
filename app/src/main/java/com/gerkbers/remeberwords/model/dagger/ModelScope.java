package com.gerkbers.remeberwords.model.dagger;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by k.bersenev on 09.11.2017.
 * com.gerkbers.remeberwords.Model.dagger
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface ModelScope {
}
