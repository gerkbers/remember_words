package com.gerkbers.remeberwords.model.dagger;

import com.gerkbers.remeberwords.model.entities.DaoSession;
import com.gerkbers.remeberwords.model.Model;

import dagger.Module;
import dagger.Provides;

/**
 * Created by k.bersenev on 09.11.2017.
 * com.gerkbers.remeberwords.Model.dagger
 */
@Module
public class ModelModule {


    @Provides
    @ModelScope
    public Model provideModel(DaoSession daoSession) {
        return new Model(daoSession);
    }
}
