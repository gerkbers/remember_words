package com.gerkbers.remeberwords.model;

import com.gerkbers.remeberwords.model.entities.DaoSession;
import com.gerkbers.remeberwords.model.entities.Theme;
import com.gerkbers.remeberwords.model.entities.ThemeDao;
import com.gerkbers.remeberwords.model.entities.Word;
import com.gerkbers.remeberwords.model.entities.WordDao;

import org.greenrobot.greendao.query.QueryBuilder;
import org.greenrobot.greendao.query.WhereCondition;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;

import static com.gerkbers.remeberwords.Utils.createObservable;

/**
 * Created by k.bersenev on 09.11.2017.
 * com.gerkbers.remeberwords.Model
 */

public class Model {

    DaoSession mDaoSession;

    public Model(DaoSession daosession) {
        mDaoSession = daosession;
    }

    private List<OnChangeThemesListener> themeListeners = new ArrayList<>();

    public void addThemeListener(OnChangeThemesListener listener) {
        themeListeners.add(listener);
    }

    public void removeThemeListener(OnChangeThemesListener listener) {
        themeListeners.remove(listener);
    }

    public Observable<List<Theme>> getThemes() {
        return createObservable(e -> {
            e.onNext(mDaoSession.getThemeDao().loadAll());
            e.onComplete();
        });
    }

    public Observable<Boolean> addTheme(String themeStr) {
        return createObservable((ObservableOnSubscribe<Boolean>)e -> {
            Theme theme = new Theme(themeStr);
            long id = mDaoSession.getThemeDao().insert(theme);
            e.onNext(true);
            e.onComplete();
        }).doOnNext(this::notifyChangeTheme);
    }

    public Observable<Boolean> updateTheme(Theme theme) {
        return createObservable(e -> {
            mDaoSession.getThemeDao().update(theme);
            e.onNext(true);
            e.onComplete();
        });
    }



    public Observable<Boolean> removeTheme(long id) {
        return createObservable((ObservableOnSubscribe<Boolean>)e -> {
            mDaoSession.getThemeDao()
                    .queryBuilder()
                    .where(ThemeDao.Properties.Id.eq(id))
                    .buildDelete()
                    .executeDeleteWithoutDetachingEntities();

            mDaoSession.clear();
            e.onNext(true);
            e.onComplete();
        }).doOnNext(this::notifyChangeTheme);
    }

    public Observable<Theme> getThemeById(long id) {
        return createObservable(e -> {
            Theme theme = mDaoSession.getThemeDao()
                    .queryBuilder()
                    .where(ThemeDao.Properties.Id.eq(id))
                    .build()
                    .unique();
            if(theme == null) {
                e.onError(new Error("No theme"));
            }
            else {
                e.onNext(theme);
            }
            e.onComplete();
        });
    }

    public Observable<Word> getWordById(long id) {
        return createObservable(e -> {
            Word word = mDaoSession.getWordDao()
                    .queryBuilder()
                    .where(WordDao.Properties.Id.eq(id))
                    .build()
                    .unique();
            if(word == null) {
                e.onError(new Exception("word == null"));
            }
            else {
                e.onNext(word);
            }
            e.onComplete();
        });
    }

    public Observable<Boolean> addWord(Word word) {
        return createObservable(e -> {
            mDaoSession.getWordDao()
                    .insert(word);
            e.onNext(true);
            e.onComplete();
        });
    }

    public Observable<Boolean> removeWord(long id) {
        return createObservable(e -> {
            mDaoSession.getWordDao()
                    .queryBuilder()
                    .where(WordDao.Properties.Id.eq(id))
                    .buildDelete()
                    .executeDeleteWithoutDetachingEntities();
            mDaoSession.clear();
            e.onNext(true);
            e.onComplete();
        });
    }

    public Observable<List<Word>> getExamWords(int quantity, List<Theme> themes) {
        return createObservable(e -> {
            List<WhereCondition> conditions = new ArrayList<>();
            for(Theme theme : themes) {
                if(theme.getIsChecked()) {
                    conditions.add(WordDao.Properties.MTheme.eq(theme.getId()));
                }
            }
            QueryBuilder<Word> queryBuilder = mDaoSession.getWordDao()
                    .queryBuilder()
                    .orderAsc(WordDao.Properties.MTimeLastRepeat)
                    .limit(quantity);
            if(conditions.size() == 1) {
                queryBuilder.where(conditions.get(0));
            }
            else if(conditions.size() == 2) {
                queryBuilder.whereOr(conditions.get(0), conditions.get(1));
            }
            else if(conditions.size() > 2) {
                WhereCondition[] whereConditions = new WhereCondition[conditions.size() - 2];
                for(int i = 2; i < conditions.size(); i++) {
                    whereConditions[i - 2] = conditions.get(i);
                }
                queryBuilder.whereOr(conditions.get(0), conditions.get(1), whereConditions);
            }


            e.onNext(queryBuilder.list());
            e.onComplete();

        });
    }

    public Observable<Boolean> updateWord(Word word) {
        return createObservable(e -> {
            mDaoSession.getWordDao().update(word);
            e.onNext(true);
            e.onComplete();
        });
    }

    private void notifyChangeTheme(Boolean success) {
        for(OnChangeThemesListener listener : themeListeners) {
            listener.onChangeThemes();
        }
    }

    public interface OnChangeThemesListener{
        void onChangeThemes();
    }
}
