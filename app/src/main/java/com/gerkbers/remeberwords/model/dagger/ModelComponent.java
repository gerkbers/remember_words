package com.gerkbers.remeberwords.model.dagger;

import com.gerkbers.remeberwords.presenter.exam_user_presenter.dagger.ExamUserComponent;
import com.gerkbers.remeberwords.presenter.exam_user_presenter.dagger.ExamUserModule;
import com.gerkbers.remeberwords.presenter.main_presenter.dagger.CheckUserComponent;
import com.gerkbers.remeberwords.presenter.main_presenter.dagger.CheckUserModule;
import com.gerkbers.remeberwords.presenter.edit_themes_presenter.dagger.EditThemesComponent;
import com.gerkbers.remeberwords.presenter.edit_themes_presenter.dagger.EditThemesModule;
import com.gerkbers.remeberwords.presenter.edit_words_presenter.dagger.EditWordsComponent;
import com.gerkbers.remeberwords.presenter.edit_words_presenter.dagger.EditWordsModule;

import dagger.Subcomponent;

/**
 * Created by k.bersenev on 09.11.2017.
 * com.gerkbers.remeberwords.Model.dagger
 */
@ModelScope
@Subcomponent(modules = {ModelModule.class})
public interface ModelComponent {

    CheckUserComponent plus(CheckUserModule checkUserModule);

    EditThemesComponent plus(EditThemesModule module);

    EditWordsComponent plus(EditWordsModule module);

    ExamUserComponent plus(ExamUserModule module);
}
