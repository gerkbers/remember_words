package com.gerkbers.remeberwords

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import butterknife.ButterKnife
import butterknife.Unbinder
import com.gerkbers.remeberwords.view.edit_themes.EditThemesFragment
import com.gerkbers.remeberwords.view.edit_themes.AddThemeDialog
import com.gerkbers.remeberwords.view.edit_words.AddWordsDialog
import com.gerkbers.remeberwords.view.edit_words.EditWordsDialog
import com.gerkbers.remeberwords.view.edit_words.EditWordsFragment
import com.gerkbers.remeberwords.view.exam_user.ExamUserFragment
import com.gerkbers.remeberwords.view.main.MainActivityFragment

class MainActivity() : AppCompatActivity(), Router {
    override fun setTitleHeader(titleRes: Int) {
        setTitle(titleRes)
    }

    override fun setTitleHeader(title: String?) {
        setTitle(title)
    }

    override fun showEditWordDialog() {
        EditWordsDialog().show(supportFragmentManager, "dialogEditWord")
    }

    override fun backToMain() {
        onBackPressed()
    }

    override fun showAddWordDialog() {
        AddWordsDialog().show(supportFragmentManager, "dialogAddWord")
    }

    override fun showThemeWords(themeID: Long) {
        themeIdNow = themeID
        replaceFragment(EditWordsFragment(), true, true)
    }


    override fun getThemeId(): Long = themeIdNow



    override fun showExam() {
        replaceFragment(ExamUserFragment(), true, true)
    }

    override fun showAddThemeDialog() {
        AddThemeDialog().show(supportFragmentManager, "dialogAddTheme")
    }

    lateinit var unbinder : Unbinder

    var themeIdNow : Long = 0

    val TAG : String = "containerFragment"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        unbinder = ButterKnife.bind(this)
        setContentView(R.layout.activity_main)
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        if(supportFragmentManager.findFragmentById(R.id.fragmentContainer) == null) {
            replaceFragment(MainActivityFragment(), false, false)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        unbinder.unbind()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId


        if (id == R.id.action_settings) {
            val fragment = EditThemesFragment()
            replaceFragment(fragment, true, true)
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    fun replaceFragment(fragment : Fragment, addBackTrace : Boolean, withAnimation: Boolean ) {
        val transaction = supportFragmentManager.beginTransaction()
        if(withAnimation) {
            transaction.setCustomAnimations(R.anim.slide_in_from_right, R.anim.slide_out_to_left,
                    R.anim.slide_in_from_left, R.anim.slide_out_to_right)
        }
        if(addBackTrace) transaction.addToBackStack(null)
        transaction.replace(R.id.fragmentContainer, fragment, TAG)
        transaction.commit()
    }


}
